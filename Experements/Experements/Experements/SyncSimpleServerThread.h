//#pragma once
//#include <algorithm>
//
//#include "Shortcuts.h"
//
//
//class Client
//{
//private:
//	socket_ptr socket_;
//	std::shared_ptr<char[]> buff_;
//	int already_read_{false};
//	int size_buff_{ 0 };
//public:
//	Client(io_context& context, int size_buff = 1024)
//	{
//		socket_.reset(new ip_tcp::socket(context));
//		buff_.reset(new char[size_buff]);
//		size_buff_ = size_buff;
//	}
//	/// Getters area
//	ip_tcp::socket* get_socket()
//	{
//		return socket_.get();
//	}
//	int get_size_buff()
//	{
//		return size_buff_;
//	}
//	int get_already_read()
//	{
//		return already_read_;
//	}
//	char* get_buffer()
//	{
//		return buff_.get();
//	}
//	/// Setters area
//	void set_already_read(int count)
//	{
//		already_read_ = count;
//	}
//
//};
//
//class SyncSimpleServerThread
//{
//private:
//	std::vector<Client> clients;
//public:
//	void handle_clients()
//	{
//		while (true)
//		{
//			for (int i = 0; i < clients.size(); ++i)
//			{
//				if (clients[i].get_socket()->available())
//					on_read(clients[i]);
//			}
//		}
//	}
//	void on_read(Client& client)
//	{
//		int to_read = std::min(size_t(client.get_size_buff() - client.get_already_read()), client.get_socket()->available());
//		client.get_socket()->read_some(buffer(client.get_buffer() + client.get_already_read(), to_read));
//		client.set_already_read(client.get_already_read() + to_read);
//		if (std::find(client.get_buffer(), client.get_buffer() + client.get_already_read(), '\n') < client.get_buffer() + client.get_already_read())
//		{
//			int pos = std::find(client.get_buffer(), client.get_buffer() + client.get_already_read(), '\n') - client.get_buffer();
//			std::string msg(client.get_buffer(), client.get_buffer() + pos);
//			std::copy(client.get_buffer() + pos, client.get_buffer() + client.get_size_buff(), client.get_buffer());
//			client.set_already_read(client.get_already_read() - pos);
//			on_read_msg(client, msg);
//		}
//	}
//	void on_read_msg(Client& client, const std::string& msg)
//	{
//		if (msg == "login")
//			client.get_socket()->write_some("login_ok");
//		else
//			client.get_socket()->write_some("login_fail");
//	}
//};