#include <iostream>
#include <iterator>
#include <algorithm>
#include <string> 
#include <chrono>

#include "Syncronus.h"
#include "Asyncronus.h"
#include "Resolver.h"
#include "SyncClient.h"
#include "SyncSimpleServerThread.h"
#include "Straand.h"
#include "Client_Server_example/Echo_Client.h"
#include "Client_Server_example/Echo_Server.h"
#include "Client_Server_example/Echo_client_async.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
template <class T>
void server_simple_example(int port = 56788)
{
	T server_example(ip_tcp::v4(), port);
	server_example.run();
}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void client_simple_example(int port = 56788)
{
	SyncSimpleClientExample simple_client_example(boost::asio::ip::address_v4::loopback().to_string(), port);
	std::string msg = "Ooops, I did it again =)";
	for (int i = 0; i < 5; ++i)
	{
		simple_client_example.send(msg);
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void resolver_example(std::string& name_host)
{
	Resolver resolver_example(name_host);
	std::string ip_resolve_string = resolver_example.get_ip_address();
	std::cout << "ip address of "
		<< name_host
		<< " is "
		<< ip_resolve_string
		<< std::endl;
}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void strand_with_thread_example()
{
	Restaraunt restaraunt;
	restaraunt.new_client();
	restaraunt.new_client();
	restaraunt.new_client();
	restaraunt.start_service();
}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void echo_client_example(int port = 56788)
{
	std::vector<std::string> messages = {
		"John says hi",
		"so does James",
		"Lucy just got home",
		"Boost.Asio is Fun!"
	};
	std::vector<std::thread> threads;
	for (std::string& msg : messages)
	{
		std::shared_ptr<EchoClient> echo_client(new EchoClient(boost::asio::ip::address_v4::loopback().to_string()));
		threads.push_back(std::thread(&EchoClient::sync_echo, std::move(echo_client), msg));
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
	for (std::thread& thread : threads)
	{
		thread.join();
	}
}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void echo_async_client_example(int port = 56789)
{
	std::vector<std::string> messages = {
		"John says hi",
		"so does James",
		"Lucy just got home",
		"Boost.Asio is Fun!"
	};

	for (std::string& msg : messages)
	{
		std::shared_ptr<EchoClientAsync> new_client = EchoClientAsync::start(msg);
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		break;
	}
	context_.run();
}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void echo_server_example(int port = 56789)
{
	EchoServer echo_server_example(port);
}
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/

int main()
{
	std::cout << "Choose: \n 1 - SyncServer \n 2 - AsyncServer server \n 3 - Resolver host name" 
		      << std::endl;
	int choose_version = 9;  // 0
	while (choose_version > 9 || choose_version < 1)
	{
		std::cout << "Your choose >> ";
		std::cin >> choose_version;
		std::cout << std::endl;
	}
	switch (choose_version)
	{
		case 1:
		{
			server_simple_example<SyncSimpleExample>();
			break;
		}
		case 2:
		{
			server_simple_example<AsyncSimpleExample>();
			break;
		}
		case 3:
		{
			resolver_example(std::string("www.google.com"));
			break;
		}
		case 4:
		{
			client_simple_example();
			break;
		}
		case 5:
		{
			// In this section will be example for Sync and Async More complex server
			// SyncSimpleServerThread class e.t.c
			break;
		}
		case 6:
		{
			strand_with_thread_example();
			break;
		}
		case 7:
		{
			echo_client_example();
			break;
		}
		case 8:
		{
			echo_async_client_example();
			break;
		}
		case 9:
		{
			echo_server_example();
			break;
		}
	}
	std::cout << std::endl;
	std::cout << "This is end of example programm. \n Please, enter \'exit\'!" << std::endl;
	
	std::string exit_str{""};
	while (exit_str.compare("exit") != 0)
	{
		std::cout << "Print exit >> ";
		std::cin >> exit_str;
		std::transform(exit_str.begin(), exit_str.end(), exit_str.begin(), ::tolower);
	}
}
