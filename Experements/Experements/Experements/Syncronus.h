#pragma once
#include "Shortcuts.h"

/* 
	� ���� ������ �� ����� ��������� ���������� ������� �������.
	��, ��, ������� � ��� � ������ �������� �����������.
	������ � ��� ��� �� ��������. ������� � ����� run. �� ��������
	����������� � ����� � ��� ������ ����� ����� ������� ����������,
	�� ����� ��������� ��� ������ �� ����� �����, � ��� ������������
	(����� ������) ����� ���� �� �����. ����� �� ����� ������ ������,
	���� ��� ���� � ���������� - 'Ok' - ������� ���� ��� ������ �������.

	� ���� ���������� �� ������������� �� ������ �� ������ �������� ���, 
	���� �������� ��� ������������ ������ ���� ������� ���������.
*/
class SyncSimpleExample
{
private:
    io_context context_;
	socket_ptr socket_;
	endpoint_ptr endpoint_;
	acceptor_ptr acceptor_;

	AsioError::asio_throw_function throw_error_;

public:
    SyncSimpleExample(ip_tcp& tcp_, 
				      int port
	)
    {
		endpoint_.reset(new ip_tcp::endpoint(tcp_, port));
		acceptor_.reset(new ip_tcp::acceptor(context_, *endpoint_.get()));
		throw_error_ = AsioError::throwAsioError;
    }
    void run()
    {
        while (true)
        {
			socket_.reset(new ip::tcp::socket(context_));
			acceptor_->accept(*socket_);
            std::thread thread_socket(std::bind(&SyncSimpleExample::client_session, this, std::move(socket_)));
			thread_socket.detach();
        }
    }
	
private:
    void client_session(socket_ptr sock)
    {
		try
		{
			char data[512]{0};
			size_t len = sock->read_some(buffer(data));
			std::cout << data << std::endl;
			if (len > 0)
				write(*sock, buffer("Ok", 2));
		}
		catch (const AsioError::WrapperAsioError& exception)
		{
			std::cout << "I catch, stop, what i caught ... " << exception.what() << std::endl;
		}
    }
};