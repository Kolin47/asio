#pragma once
#include "Shortcuts.h"

class SyncSimpleClientExample
{
private:
	io_context context_;
	socket_ptr socket_;
	endpoint_ptr endpoint_;

	AsioError::asio_throw_function throw_error_;

public:
	SyncSimpleClientExample(std::string& str_ip_adress,
		int port
	)
	{
		endpoint_.reset(new ip_tcp::endpoint(ip::address::from_string(str_ip_adress), port));
		socket_.reset(new ip_tcp::socket(context_));
		

		throw_error_ = AsioError::throwAsioError;
	}
	void send(std::string& send_msg)
	{
		socket_->open(ip_tcp::v4());
		socket_->connect(*endpoint_);
		socket_->write_some(buffer(send_msg));
		std::cout << "bytes available " << socket_->available() << std::endl;
		char data[512]{ 0 };
		socket_->read_some(buffer(data,512));
		socket_->close();
	}	
};