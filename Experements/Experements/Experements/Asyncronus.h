#pragma once
#include "Shortcuts.h"

class AsyncSimpleExample
{
private:
	io_context context_;
	socket_ptr socket_;
	endpoint_ptr ep_;
	acceptor_ptr acc_;

	AsioError::asio_throw_function throw_error_;

public:
	AsyncSimpleExample(ip_tcp& tcp_, 
					   int port
	)
	{
		ep_.reset(new ip_tcp::endpoint(tcp_, port));
		acc_.reset(new ip_tcp::acceptor(context_, *ep_.get()));
		throw_error_ = AsioError::throwAsioError;
	}
	void run()
	{
		socket_.reset(new ip_tcp::socket(context_));
		start_accept(socket_);
		context_.run();
	}

private:
	void start_accept(socket_ptr socket)
	{
		acc_->async_accept(*socket, std::bind(&AsyncSimpleExample::handle_accept, this, socket, std::placeholders::_1));
	}

	void handle_accept(socket_ptr sock, 
					   const boost::system::error_code& err
	)
	{
		if (err) return;

		try
		{
			char data[512]{0};
			size_t len = sock->read_some(buffer(data));
			std::cout << data << std::endl;
			if (len > 0)
				write(*sock, buffer("Ok", 2));

			/* This block code show how to use exception in this library*/
			//-----------------------------------------------------
			// throw_error_("You caught me sweety", 0);
			//-----------------------------------------------------
			socket_.reset(new ip_tcp::socket(context_));
			start_accept(socket_);
		}
		catch (const AsioError::WrapperAsioError& exception)
		{
			std::cout << "I catch, stop, what i caught ... " << exception.what() << std::endl;
		}
	}
};