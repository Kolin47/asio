#pragma once
#include <chrono>

#include "Shortcuts.h"

class Person : std::enable_shared_from_this<Person>
{
private:
	int speed_{0};
	int id_{ 0 };
public:
	Person(int speed, int id)
		: speed_(speed)
		, id_(id)
	{}
	void order() {

		sync_cout << "Person " + std::to_string(id_) + " is ordering \n";
		std::this_thread::sleep_for(std::chrono::seconds(speed_));	
		sync_cout << "Person " + std::to_string(id_) + " finish ordering \n";
	};
	void eat() {
		sync_cout << "Person " + std::to_string(id_) + " is eating \n";
		std::this_thread::sleep_for(std::chrono::seconds(speed_));
		sync_cout << "Person " + std::to_string(id_) + " finish eating \n";
	}
	void pay() {
		sync_cout << "Person " + std::to_string(id_) + " is paying \n";
		std::this_thread::sleep_for(std::chrono::seconds(speed_));
		sync_cout << "Person " + std::to_string(id_) + " finish paying \n";
	}

	std::shared_ptr<Person> getptr() {
		return shared_from_this();
	}
};

class Restaraunt
{
private:
	io_context context_;
	std::vector<std::shared_ptr<Person>> vec_persons_;
	std::vector<std::thread> vec_threads_;

	void run() {
		context_.run();
	}
public:
	void start_service() {
		for (std::shared_ptr<Person> person : vec_persons_)
		{
			io_context::strand _strand(context_);
			context_.post(_strand.wrap(std::bind(&Person::order, person)));
			context_.post(_strand.wrap(std::bind(&Person::eat, person)));
			context_.post(_strand.wrap(std::bind(&Person::pay, person)));
		}
		for (int i = 0; i < 3; ++i)
		{
			vec_threads_.push_back(std::thread(&Restaraunt::run, this));
		}
		for (std::thread& _thread : vec_threads_)
		{
			_thread.join();
		}
	}

	void new_client()
	{
		vec_persons_.push_back(std::shared_ptr<Person>(new Person(5, vec_persons_.size() + 1)));
	}
};