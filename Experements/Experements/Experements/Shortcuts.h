#pragma once
#include <memory>
#include <algorithm> 
#include <boost/asio.hpp>

using namespace boost::asio;

typedef ip::tcp ip_tcp;
typedef ip_tcp::resolver ip_resolver;

using socket_ptr = std::shared_ptr<ip::tcp::socket>;
using acceptor_ptr = std::shared_ptr<ip_tcp::acceptor>;
using endpoint_ptr = std::shared_ptr<ip_tcp::endpoint>;
using resolver_ptr = std::shared_ptr<ip_tcp::resolver>;
using boost_error_code = boost::system::error_code;

namespace AsioError
{
	class WrapperAsioError : public std::exception
	{
	private:
		std::string what_{"Undefine error"};
		int			code_{ 256 };
	public:
		WrapperAsioError(std::string what, 
						 int code
		)
			:what_(what)
			,code_(code)
		{}
		virtual const char* what() const throw()
		{
			return what_.c_str();
		}
		const virtual int code() const throw()
		{
			return code_;
		}
	};
	

	typedef void(*asio_throw_function)(const std::string &msg, int error);
	typedef void(*asio_throw_function)(const std::string &msg, int error);

	static void throwAsioError(const std::string &msg, 
							   int error = 0
	)
	{
		WrapperAsioError wrapper_asio_error(msg, error);
		throw wrapper_asio_error;
	}
}

class Sync_cout
{
private:
	std::mutex mt_stream_out_;
public:
	Sync_cout()
	{
	}
	template <class T>
	void operator<<(const T& str)
	{
		std::lock_guard<std::mutex> lock(mt_stream_out_);
		std::cout << str;
	}
};
Sync_cout sync_cout;


class DafaultBuffer : std::enable_shared_from_this<DafaultBuffer>
{
private:
	DafaultBuffer() = delete;
private:
	using shared_buffer = std::shared_ptr<std::string>;
	using shared_this   = std::shared_ptr<DafaultBuffer>;
	shared_buffer buffer_;
	int			size_buffer_{ 1 };
public:
	explicit DafaultBuffer(int size)
		: size_buffer_(size)
	{
		buffer_.reset(new std::string(""));
		buffer_->resize(size_buffer_);
		std::fill(buffer_->begin(), buffer_->end(), '\0');
	}
	std::string& get_buffer()
	{
		return *buffer_;
	}
	shared_buffer get_shared_buffer()
	{
		return buffer_;
	}
};

class Buffers 
{
public:
	std::shared_ptr<DafaultBuffer> write_buffer_;
	std::shared_ptr<DafaultBuffer> read_buffer_;
	Buffers(
		size_t size_read = 1024, 
		size_t size_write = 1024
	)
	{
		write_buffer_.reset(new DafaultBuffer(size_write));
		read_buffer_.reset(new DafaultBuffer(size_read));
	}
};

