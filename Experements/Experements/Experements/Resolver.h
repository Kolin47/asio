#pragma once
#include "Shortcuts.h"


class Resolver
{
private:
	io_context context_;
	socket_ptr socket_;
	endpoint_ptr endpoint_;
	resolver_ptr resolver_;
	int port_{80};
	const std::string& server_name_;
public:
	Resolver(const std::string& server_name, 
			 int port = 80
	)
		: server_name_(server_name)
		, port_(port)
	{}
	std::string get_ip_address()
	{
		resolver_.reset(new ip_resolver(context_));
		ip_resolver::query _query(server_name_, std::to_string(port_));
		ip_resolver::iterator _iterator = resolver_->resolve(_query);
		endpoint_.reset(new ip_tcp::endpoint());
		*endpoint_ = *_iterator;
		return endpoint_->address().to_string();
	}
};