#pragma once

#include <string>

#include "../Shortcuts.h"

class EchoClient : std::enable_shared_from_this<EchoClient>
{
private:
	endpoint_ptr endpoint_;
	socket_ptr   socket_;
	io_context   context_;

	std::shared_ptr<DafaultBuffer> buffer_;

	size_t read_complete(
		std::shared_ptr<std::string> buf,
		const boost::system::error_code& err,
		size_t bytes
	)
	{
		if (err) 
			return 0;
		return buf->find('\n') == std::string::npos ? 1 : 0;
	}	
public:
	EchoClient(
		std::string ip_address,
		int port = 56789)
	{
		buffer_.reset(new DafaultBuffer(1024));
		endpoint_.reset(new ip_tcp::endpoint(ip::address::from_string(ip_address), port));	
	}
	void sync_echo(std::string& msg)
	{
		if (*(msg.end() - 1) != '\n')
		{
			msg.append("\n");
		}
		try
		{
			socket_.reset(new ip_tcp::socket(context_));
			socket_->connect(*endpoint_);
			socket_->write_some(buffer(msg));	
			int bytes = read(*socket_.get(),
				buffer(buffer_->get_buffer()),
				std::bind(
					&EchoClient::read_complete,
					this,
					buffer_->get_shared_buffer(),
					std::placeholders::_1,
					std::placeholders::_2
				)
			);
			socket_->close();	
			std::cout << "server echoed our " << msg << ": " << (buffer_->get_buffer() == msg ? "OK" : "FAIL") << std::endl;
		}
		catch(std::runtime_error& err)
		{
			std::cout << err.what() << std::endl;
		}		
	}

	std::shared_ptr<EchoClient> getptr() {
		return shared_from_this();
	}
};