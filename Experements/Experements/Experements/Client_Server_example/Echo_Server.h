#pragma once
#include "../Shortcuts.h"

class DafaultBuffer;
using namespace std::placeholders;
class EchoServer : std::enable_shared_from_this<EchoServer>
{
private:
	endpoint_ptr endpoint_;
	acceptor_ptr acceptor_;
	io_context	 context_;
	socket_ptr   socket_;

	std::shared_ptr<DafaultBuffer> buffer_;

	int port_{ 0 };

	void handle_connection()
	{	
		endpoint_.reset(new ip_tcp::endpoint(ip_tcp::v4(), port_));
		acceptor_.reset(new ip_tcp::acceptor(context_, *endpoint_));
		while (true)
		{
			socket_.reset(new ip_tcp::socket(context_));
			acceptor_->accept(*socket_);
			size_t bytes = read(
				*socket_,
				buffer(buffer_->get_buffer()),
				std::bind(
					&EchoServer::read_complete, 
					this, 
					buffer_->get_shared_buffer(),
					_1, 
					_2
				)
			);
			socket_->write_some(buffer(buffer_->get_buffer()));
			socket_->close();
		}
	}

	size_t read_complete(std::shared_ptr<std::string> buf, const boost::system::error_code& err, size_t bytes)
	{
		if (err) return 0;
		bool found = buf->find('\n') != std::string::npos;
		return found ? 0 : 1;
	}

public:
	EchoServer(
		int port = 56789
	) 
		: port_(port)
	{
		buffer_.reset(new DafaultBuffer(1024));
		handle_connection();
	}
};