#pragma once

#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "../Shortcuts.h"

using namespace std::placeholders;

using string_ptr = std::shared_ptr<std::string>;
io_context context_;

class EchoClientAsync : public std::enable_shared_from_this<EchoClientAsync>
{
private:

	using this_ptr = std::shared_ptr<EchoClientAsync>;
	using self_type = EchoClientAsync;

	socket_ptr   socket_;
	endpoint_ptr endpoint_;

	struct config
	{
		std::string& ip_address_;
		int port_;
		config(std::string& ip_address, int port)
			: ip_address_(ip_address),
			port_(port)
		{}
	};
	std::shared_ptr<config> config_;

	std::shared_ptr<Buffers> buffers_;

	string_ptr message_;

	this_ptr ptr_;

	std::atomic<bool> started_{false};

	EchoClientAsync(const EchoClientAsync& other) = delete;
	EchoClientAsync& operator=(const EchoClientAsync&) = delete;

	EchoClientAsync(
		std::string& ip_address,
		int port
	)
	{
		buffers_.reset(new Buffers());
		config_.reset(new config(ip_address, port));
		endpoint_.reset(new ip_tcp::endpoint(
			ip::address::from_string(config_->ip_address_),
			config_->port_)
		);
		socket_.reset(new ip_tcp::socket(context_));
	}
	void stop()
	{
		if (!started_)
			return;
		started_ = false;
		socket_->close();
	}	
	
	void on_connect_(const boost_error_code& err)
	{
		if (!err)
			do_write_();
		else
			stop();
	}

	void do_write_()
	{
		if (!started_)
			return;
		buffers_->write_buffer_->get_shared_buffer()->assign(message_->data(), message_->size());
		socket_->async_write_some(buffer(buffers_->write_buffer_->get_buffer()), std::bind(&self_type::on_write_, shared_from_this(), _1, _2));
	}
	void on_write_(const boost_error_code& err, size_t bytes)
	{
		do_read_();
	}
	void do_read_()
	{
		async_read(*socket_,
			buffer(buffers_->read_buffer_->get_buffer()),
			std::bind(&self_type::read_complete_, shared_from_this(), _1, _2),
			std::bind(&self_type::on_read_, shared_from_this(), _1, _2)
			);
	}
	void on_read_(const boost_error_code& err, size_t bytes)
	{
		if (!err)
		{
			std::cout << "server echoed our " << message_->data() << ": "<< buffers_->read_buffer_->get_buffer() << std::endl;
		}
		stop();
	}
	size_t read_complete_(const boost_error_code& err, size_t bytes)
	{
		if (err) return 0;
		bool found = buffers_->read_buffer_->get_shared_buffer()->find('\n') != std::string::npos;
		return found ? 0 : 1;
	}
	

public:
	~EchoClientAsync(){}
	static this_ptr start(
		std::string& msg,
		std::string& ip_address = boost::asio::ip::address_v4::loopback().to_string(),
		int port = 56789	
	)
	{	
		this_ptr ptr(new EchoClientAsync(ip_address, port));
		ptr->send_message(msg);
		return ptr;
	}

	void send_message(std::string& msg)
	{	
		started_ = true;
		message_.reset(new std::string());
		message_->assign(msg);
		if (*(message_->end() - 1) != '\n')
		{
			message_->append("\n");
		}
		socket_->async_connect(*endpoint_, std::bind(&self_type::on_connect_, shared_from_this(), _1));
	}

	this_ptr getptr() {
		return shared_from_this();
	}
};